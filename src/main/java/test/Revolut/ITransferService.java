package test.Revolut;

import javax.xml.bind.JAXBException;

public interface ITransferService {

	/**
	 * Get an Account by its ID (IBAN)
	 * @param id
	 * @return
	 * @throws JAXBException
	 */
    public Account getAccount(String id) throws JAXBException;
    
    /**
     * Add an Account, do not generate anything
     * @param account
     * @return
     * @throws JAXBException 
     */
    public Account addAccount(Account account) throws JAXBException;
    
    /**
     * Edit an Account
     * @param account
     * @return
     * @throws JAXBException 
     */
    public Account editAccount(Account account) throws JAXBException;
    
    /**
	 * Delete an Account by its ID (IBAN)
	 * @param id
	 * @return
	 * @throws JAXBException
	 */
    public boolean deleteAccount(String id) throws JAXBException;
    
    /**
     * Do a transfer between Acount
     * @param transfer
     * @return
     * @throws JAXBException
     */
	public boolean doTransfer(Transfer transfer) throws JAXBException;
}
