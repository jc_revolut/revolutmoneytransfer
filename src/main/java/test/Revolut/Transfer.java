package test.Revolut;

public class Transfer {

	private String from;
	private String to;
	private Float amount;
	private Currency currency;
	
	
	public Transfer(String from, String to, Float amount, Currency currency) {
		super();
		this.from = from;
		this.to = to;
		this.amount = amount;
		this.currency = currency;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public Currency getCurrency() {
		return currency;
	}
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}
	
	
}