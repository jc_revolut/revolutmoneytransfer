package test.Revolut;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Account {
	private String id;
	private Currency currency;
	private Float balance;
	private String client;

	public String getId() {
		return id;
	}

	@XmlAttribute
	public void setId(String id) {
		this.id = id;
	}

	public Currency getCurrency() {
		return currency;
	}

	@XmlElement
	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public Float getBalance() {
		return balance;
	}

	@XmlElement
	public void setBalance(Float balance) {
		this.balance = balance;
	}


	public String getClient() {
		return client;
	}

	@XmlElement
	public void setClient(String client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", currency=" + currency + ", balance=" + balance + ", client=" + client + "]";
	}
	
	

}
