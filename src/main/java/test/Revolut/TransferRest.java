package test.Revolut;

import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

import com.google.gson.Gson;

public class TransferRest {

	public static void main(String[] args) {
		final ITransferService transferService = new TransferService();

		// Welcome message
		get("/welcome", (req, res) -> "Welcome to the Revolut money transfer test.");

		// Get an Account
		get("/account/:id", (req, res) -> {
			res.type("application/json");

			Account account = transferService.getAccount(req.params(":id"));
			if (account == null)
				return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, "ressource not found"));
			else
				return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(account)));

		});
		
		// Edit an Account
		put("/account", (req, res) -> {
			res.type("application/json");
			Account account = new Gson().fromJson(req.body(), Account.class);
			
			try {
				if (account == null)
					throw new Exception("incorrect entry");
				account = transferService.editAccount(account);
			} catch (Exception e) {
				return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, e.getMessage()));
			}

			return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(account)));
		});

		// Add an Account
		post("/account", (req, res) -> {
			res.type("application/json");
			Account account = new Gson().fromJson(req.body(), Account.class);

			try {
				if (account == null)
					throw new Exception("incorrect entry");
				account = transferService.addAccount(account);
			} catch (Exception e) {
				return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, e.getMessage()));
			}

			return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, new Gson().toJsonTree(account)));
		});

		// Delete an Account
		delete("/account/:id", (req, res) -> {
			res.type("application/json");
			try {
				transferService.deleteAccount(req.params(":id"));
			} catch (Exception e) {
				return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, e.getMessage()));
			}
			return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS, "account deleted"));
		});
		
		// Transfer between Accounts
		post("/transfer", (req, res) -> {
		    res.type("application/json");

		    Transfer transfer = new Gson().fromJson(req.body(), Transfer.class);
			try {
			    transferService.doTransfer(transfer);
			} catch (Exception e) {
				return new Gson().toJson(new StandardResponse(StatusResponse.ERROR, e.getMessage()));
			}
		    return new Gson().toJson(new StandardResponse(StatusResponse.SUCCESS));
	});
	}
}
