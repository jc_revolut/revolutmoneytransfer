package test.Revolut;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="accounts")
@XmlAccessorType (XmlAccessType.FIELD)
public class Accounts {
	 	@XmlElement(name = "account")
	    private List<Account> accounts = null;
	 
	    public List<Account> getAccounts() {
	        return accounts;
	    }
	 
	    public void setAccounts(List<Account> accounts) {
	        this.accounts = accounts;
	    }
	    
	    public void add(Account account) {
	        if (this.accounts == null) 
	            this.accounts = new ArrayList<Account>();
	        
	        this.accounts.add(account);
	 
	    }
}
