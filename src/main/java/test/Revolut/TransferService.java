package test.Revolut;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.convert.CurrencyConversion;
import javax.money.convert.MonetaryConversions;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import spark.utils.StringUtils;

public class TransferService implements ITransferService {

	static File file = new File("src/main/java/test/Revolut/datasource.xml");

	private static final String ID_PATTERN = "[A-Z]{2}\\d{2} ?\\d{4} ?\\d{4} ?\\d{4} ?\\d{4} ?[\\d]{0,2}";

	/**
	 * Get an Account by its ID
	 */
	public Account getAccount(String id) throws JAXBException {
		return getAccounts().stream().filter(a -> a.getId().equals(id)).findFirst().orElse(null);
	}

	private List<Account> getAccounts() throws JAXBException {

		JAXBContext jaxbContext = JAXBContext.newInstance(Accounts.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

		return ((Accounts) unmarshaller.unmarshal(file)).getAccounts();
	}

	@Override
	public Account addAccount(Account account) throws JAXBException {

		if (getAccount(account.getId()) != null)
			throw new JAXBException("This IBAN already exists.");

		if (!checkId(account.getId()))
			throw new JAXBException("Wrong IBAN.");

		// check Account format
		if (StringUtils.isEmpty(account.getClient()) || account.getCurrency() == null)
			throw new JAXBException("Wrong Account format.");
		
		if (account.getBalance() == null) 
			account.setBalance(0f);

		JAXBContext jaxbContext = JAXBContext.newInstance(Accounts.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Accounts accounts = (Accounts) unmarshaller.unmarshal(file);
		accounts.add(account);

		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(accounts, file);

		return account;
	}

	
	@Override
	public Account editAccount(Account account) throws JAXBException {
		Account oldAccount = getAccount(account.getId());
		if (oldAccount == null)
			throw new JAXBException("No account found for the given IBAN.");

		if (deleteAccount(account.getId())) {
			if (StringUtils.isEmpty(account.getClient()))
				account.setClient(oldAccount.getClient());
			if (account.getBalance() == null)
				account.setBalance(oldAccount.getBalance());
			// currency cannot be changed
			account.setCurrency(oldAccount.getCurrency());
			return addAccount(account);
		}
		return null;
	}

	@Override
	public boolean deleteAccount(String id) throws JAXBException {
		if (getAccount(id) == null)
			throw new JAXBException("No account found for the given IBAN.");

		JAXBContext jaxbContext = JAXBContext.newInstance(Accounts.class);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		Accounts accounts = (Accounts) unmarshaller.unmarshal(file);
		accounts.setAccounts(
				accounts.getAccounts().stream().filter(a -> !a.getId().equals(id)).collect(Collectors.toList()));

		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		marshaller.marshal(accounts, file);

		return true;
	}

	@Override
	public boolean doTransfer(Transfer transfer) throws JAXBException {
		Account from = getAccount(transfer.getFrom());
		if (from == null) 
			throw new JAXBException("No account found for the given expeditor IBAN.");

		Account to = getAccount(transfer.getTo());
		if (to == null) 
			throw new JAXBException("No account found for the given destinator IBAN.");
		
		Float amount = transfer.getAmount();
		if (amount == null) 
			throw new JAXBException("Transfer amount not specified.");
		
		Currency currency = transfer.getCurrency();
		if (currency == null) 
			throw new JAXBException("Transfer currency not specified.");
		
		// Currency transformation
	    MonetaryAmount moneyTransfer = Monetary.getDefaultAmountFactory().setCurrency(currency.name()).setNumber(amount).create();

	    CurrencyConversion moneyTransferFromCurrency = MonetaryConversions.getConversion(from.getCurrency().name());
	    CurrencyConversion moneyTransferToCurrency = MonetaryConversions.getConversion(to.getCurrency().name());

	    MonetaryAmount convertedTransferFromCurrency = moneyTransfer.with(moneyTransferFromCurrency);
	    MonetaryAmount convertedTransferToCurrency = moneyTransfer.with(moneyTransferToCurrency);

	    
		Float currentFromBalance = from.getBalance();
		if (convertedTransferFromCurrency.getNumber().floatValue() > currentFromBalance) 
			throw new JAXBException("Insufficient expeditor balance.");
		
		from.setBalance(currentFromBalance - convertedTransferFromCurrency.getNumber().floatValue());
		to.setBalance(to.getBalance() + convertedTransferToCurrency.getNumber().floatValue());
		editAccount(from);
		editAccount(to);
		
		
		return true;
	}

	private boolean checkId(String id) {
		return id.matches(ID_PATTERN);
	}

}
