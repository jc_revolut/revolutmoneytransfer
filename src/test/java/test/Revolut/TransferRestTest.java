package test.Revolut;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.io.IOException;
import java.util.Random;

import javax.xml.bind.JAXBException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

@DisplayName("Testing money transfer API")
public class TransferRestTest {

	static final String ID_CA = "CA0011112222333344445";
	static final String ID_FR = "FR00111122223333444455";
	static final String ID_PL = "PL001111222233334444";
	static final String ID_PL_2 = "PL0011112222333344442";
	static final String ID_DE = "DE00111122223333444455";
	static final String ID_DE_BAD = "DE987";
	static TransferService service;

	@BeforeAll
	public static void init() {
		service = new TransferService();
	}
	@Test
	public void transferTest() throws ClientProtocolException, IOException, JAXBException {
		// save for later
		Account iniFromAccount = service.getAccount(ID_PL);
		Account iniToAccount = service.getAccount(ID_PL_2);
		
		// actual test
		Float amount = 100f;
		Transfer transfer = new Transfer(ID_PL, ID_PL_2, amount, Currency.PLN);
		String jsonTransfer = new Gson().toJson(transfer);
		StringEntity entity = new StringEntity(jsonTransfer);
		HttpPost request = new HttpPost("http://localhost:4567/transfer");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);
		Account fromAccount = service.getAccount(ID_PL);
		Account toAccount = service.getAccount(ID_PL_2);

		//request success
		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.SUCCESS));
		assertThat(fromAccount.getBalance(), Matchers.is(iniFromAccount.getBalance() - amount));
		assertThat(fromAccount.getCurrency(), Matchers.is(iniFromAccount.getCurrency()));
		assertThat(toAccount.getBalance(), Matchers.is(iniToAccount.getBalance() + amount));
		assertThat(toAccount.getCurrency(), Matchers.is(iniToAccount.getCurrency()));
		
		// retransfer
		service.doTransfer(new Transfer(ID_PL_2, ID_PL, amount, Currency.PLN));
	}

	@Test
	public void transferTooMuchTest() throws ClientProtocolException, IOException, JAXBException {

		Float amount = 1000000f;
		Transfer transfer = new Transfer(ID_PL, ID_PL_2, amount, Currency.PLN);
		String jsonTransfer = new Gson().toJson(transfer);
		StringEntity entity = new StringEntity(jsonTransfer);
		HttpPost request = new HttpPost("http://localhost:4567/transfer");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		//request success
		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
	}
	
	@Test
	public void transferDifferentCurrencyTest() throws ClientProtocolException, IOException, JAXBException {
		// save for later
		Account iniFromAccount = service.getAccount(ID_FR);
		Account iniToAccount = service.getAccount(ID_PL_2);

		Float amount = 100f;
		Transfer transfer = new Transfer(ID_FR, ID_PL_2, amount, Currency.CAD);
		String jsonTransfer = new Gson().toJson(transfer);
		StringEntity entity = new StringEntity(jsonTransfer);
		HttpPost request = new HttpPost("http://localhost:4567/transfer");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);
		Account fromAccount = service.getAccount(ID_FR);
		Account toAccount = service.getAccount(ID_PL_2);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.SUCCESS));
		assertThat(fromAccount.getBalance(), Matchers.lessThan(iniFromAccount.getBalance()));
		assertThat(fromAccount.getCurrency(), Matchers.is(iniFromAccount.getCurrency()));
		assertThat(toAccount.getBalance(), Matchers.greaterThan(iniToAccount.getBalance()));
		assertThat(toAccount.getCurrency(), Matchers.is(iniToAccount.getCurrency()));

		// retransfer
//		service.doTransfer(new Transfer(ID_PL_2, ID_FR, amount, Currency.CAD));
	}
	@Test
	public void welcomeTest() throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpGet("http://localhost:4567/welcome");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
	}

	@Test
	public void getAccountWithGoodIdTest() throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpGet("http://localhost:4567/account/" + ID_PL);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		Account resource = retrieveResourceFromResponse(httpResponse, Account.class);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(ID_PL, Matchers.is(resource.getId()));
		assertThat("Chopin", Matchers.is(resource.getClient()));
		assertThat(Currency.PLN, Matchers.is(resource.getCurrency()));
		assertThat(Float.valueOf(15000), Matchers.is(resource.getBalance()));
	}

	@Test
	public void getAccountWithBadIdTest() throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpGet("http://localhost:4567/account/" + ID_DE_BAD);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
	}

	@Test
	public void addAccountWithGoodIdTest() throws ClientProtocolException, IOException, JAXBException {
		Account newAccount = new Account();
		newAccount.setId(ID_DE);
		newAccount.setClient("Albert Einstein");
		newAccount.setBalance(99f);
		newAccount.setCurrency(Currency.EUR);
		String jsonNewAccount = new Gson().toJson(newAccount);
		StringEntity entity = new StringEntity(jsonNewAccount);
		HttpPost request = new HttpPost("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		Account resource = retrieveResourceFromResponse(httpResponse, Account.class);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(newAccount.getId(), Matchers.is(resource.getId()));
		assertThat(newAccount.getClient(), Matchers.is(resource.getClient()));
		assertThat(newAccount.getCurrency(), Matchers.is(resource.getCurrency()));
		assertThat(newAccount.getBalance(), Matchers.is(resource.getBalance()));
		
		//delete account once Saved
		service.deleteAccount(ID_DE);
	}

	@Test
	public void addAccountWithBadIdTest() throws ClientProtocolException, IOException {
		Account newAccount = new Account();
		newAccount.setId(ID_DE_BAD);
		newAccount.setClient("Coco Channel");
		newAccount.setBalance(999f);
		newAccount.setCurrency(Currency.EUR);
		String jsonNewAccount = new Gson().toJson(newAccount);
		StringEntity entity = new StringEntity(jsonNewAccount);
		HttpPost request = new HttpPost("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));

	}

	@Test
	public void addAccountWithExistingIdTest() throws ClientProtocolException, IOException {
		Account newAccount = new Account();
		newAccount.setId(ID_FR);
		newAccount.setClient("Coco Channel");
		newAccount.setBalance(999f);
		newAccount.setCurrency(Currency.EUR);
		String jsonNewAccount = new Gson().toJson(newAccount);
		StringEntity entity = new StringEntity(jsonNewAccount);
		HttpPost request = new HttpPost("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
	}

	@Test
	public void addAccountWithBadAccountTest() throws ClientProtocolException, IOException {
		Account newAccount = new Account();
		newAccount.setId(ID_FR);
		String jsonNewAccount = new Gson().toJson(newAccount);
		StringEntity entity = new StringEntity(jsonNewAccount);
		HttpPost request = new HttpPost("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
		
	}

	@Test
	public void editAccountWithGoodIdTest() throws ClientProtocolException, IOException {
		Account updateAccount = new Account();
		updateAccount.setId(ID_CA);
		updateAccount.setBalance(new Random().nextFloat() * (9990) + 10);
		updateAccount.setClient("Jim Carrey");
		updateAccount.setCurrency(Currency.CAD);
		String jsonUpdateAccount = new Gson().toJson(updateAccount);
		StringEntity entity = new StringEntity(jsonUpdateAccount);
		HttpPut request = new HttpPut("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		Account resource = retrieveResourceFromResponse(httpResponse, Account.class);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(updateAccount.getId(), Matchers.is(resource.getId()));
		assertThat(updateAccount.getClient(), Matchers.is(resource.getClient()));
		assertThat(updateAccount.getCurrency(), Matchers.is(resource.getCurrency()));
		assertThat(updateAccount.getBalance(), Matchers.is(resource.getBalance()));
		
	}
	@Test
	public void editAccountWithNotFullAccountTest() throws ClientProtocolException, IOException, JAXBException {
		Account iniAccount = service.getAccount(ID_CA);
		
		Account updateAccount = new Account();
		updateAccount.setId(ID_CA);
		updateAccount.setBalance(new Random().nextFloat() * (9990) + 10);
		String jsonUpdateAccount = new Gson().toJson(updateAccount);
		StringEntity entity = new StringEntity(jsonUpdateAccount);
		HttpPut request = new HttpPut("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		Account resource = retrieveResourceFromResponse(httpResponse, Account.class);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(updateAccount.getId(), Matchers.is(resource.getId()));
		assertThat(iniAccount.getClient(), Matchers.is(resource.getClient()));
		assertThat(iniAccount.getCurrency(), Matchers.is(resource.getCurrency()));
		assertThat(updateAccount.getBalance(), Matchers.is(resource.getBalance()));
	}

	@Test
	public void editAccountWithBadIdTest() throws ClientProtocolException, IOException {

		Account updateAccount = new Account();
		updateAccount.setId(ID_DE_BAD);
		updateAccount.setBalance(new Random().nextFloat() * (9990) + 10);
		String jsonUpdateAccount = new Gson().toJson(updateAccount);
		StringEntity entity = new StringEntity(jsonUpdateAccount);
		HttpPut request = new HttpPut("http://localhost:4567/account");
		request.setEntity(entity);
		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
	}


	@Test
	public void deleteAccountWithGoodIdTest() throws ClientProtocolException, IOException, JAXBException {
		// save Account for later
		Account savedAccount = service.getAccount(ID_FR);
		HttpUriRequest request = new HttpDelete("http://localhost:4567/account/" + ID_FR);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.SUCCESS));

		// re-add saved Account once deleted
		service.addAccount(savedAccount);
	}
	
	@Test
	public void deleteAccountWithBadIdTest() throws ClientProtocolException, IOException {

		HttpUriRequest request = new HttpDelete("http://localhost:4567/account/" + ID_DE_BAD);

		HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
		StandardResponse response = retrieveStandardResponse(httpResponse);

		assertThat(httpResponse.getStatusLine().getStatusCode(), equalTo(HttpStatus.SC_OK));
		assertThat(response.getStatus(), Matchers.is(StatusResponse.ERROR));
	}

	public static StandardResponse retrieveStandardResponse(HttpResponse response) throws ParseException, IOException {
		String jsonFromResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
		return new Gson().fromJson(jsonFromResponse, StandardResponse.class);
	}

	public static <T> T retrieveResourceFromResponse(HttpResponse response, Class<T> clazz) throws IOException {
		StandardResponse standardResponse = retrieveStandardResponse(response);
		return new Gson().fromJson(standardResponse.getData(), clazz);
	}
}